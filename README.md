Need to create a Maven-based project – Sorting App. It is a small Java application that takes up to ten command-line arguments as integer values, sorts them in the ascending order, and then prints them into standard output.
The project must pass the following criteria:
1 Create a Maven project and specify its GAV settings, encoding, language level, etc.
2 Write the code implementing the app specification.
3 Configure the Maven project to build a runnable jar containing application and its dependencies.
4 Share the project using a public GitLab repository.