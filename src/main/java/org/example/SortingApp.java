package org.example;

import java.util.Arrays;

public class SortingApp {
    public int[] sorting(int[] sortingArr){
        if(sortingArr.length >= 10) throw new ArrayIndexOutOfBoundsException();
        else if (sortingArr.length < 9) { throw new IllegalArgumentException(); }
        else if (sortingArr == null) { throw new NullPointerException(); }
        Arrays.sort(sortingArr);
        return sortingArr;
    }
}
