package org.example;

import org.junit.Before;
import org.junit.Test;

public class ArrayHasLessThanTenArguments {
    SortingApp sorting = new SortingApp();
    private int []argumentsMoreThenTen;

    @Before
    public void beforeMethod(){
        argumentsMoreThenTen = new int [] {42,7,2,4,3,1,0,7};
    }

    @Test(expected = IllegalArgumentException.class)
    public void argumentsMoreThenTenTest() {
        sorting.sorting(argumentsMoreThenTen);
    }
}
