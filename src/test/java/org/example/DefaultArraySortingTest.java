package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;
import java.util.stream.Stream;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class DefaultArraySortingTest {
    private final SortingApp sortingApp = new SortingApp();
    private final int[] input;
    private final int[] expectedOutput;
    public DefaultArraySortingTest(int[] input, int[] expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> sortingTestData() {
        return Stream.of(
                new Object[]{new int[]{3, 1, 4, 5, 9, 2, 6, 5, 3}, new int[]{1, 2, 3, 3, 4, 5, 5, 6, 9}},
                new Object[]{new int[]{5, 6, 2, 1, 3, 4, 7, 9, 8}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}}
        ).toList();
    }
    @Test
    public void testSorting() {
        // Act
        int[] result = sortingApp.sorting(input);
        // Assert
        assertArrayEquals(expectedOutput, result);
    }
}
