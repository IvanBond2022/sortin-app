package org.example;

import org.junit.Before;
import org.junit.Test;

public class ArrayHasMoreThanTenArgumentsTest {
    SortingApp sorting = new SortingApp();
    private int []argumentsMoreThenTen;

    @Before
    public void beforeMethod(){
        argumentsMoreThenTen = new int [] {5,42,7,2,4,3,1,0,7,8,11};
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void argumentsMoreThenTenTest() {
        sorting.sorting(argumentsMoreThenTen);
    }
}
