package org.example;

import org.junit.Before;
import org.junit.Test;

public class ArrayNullPointerTest {
    SortingApp sorting = new SortingApp();
    private int []argumentsMoreThenTen;

    @Before
    public void beforeMethod(){
        argumentsMoreThenTen = null;
    }

    @Test(expected = NullPointerException.class)
    public void argumentsMoreThenTenTest() {
        sorting.sorting(argumentsMoreThenTen);
    }

}
